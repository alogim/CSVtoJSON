#!/usr/bin/python

from optparse import OptionParser

class Converter:
    def __init__(self, input_file_path):
        self.input_file_path = input_file_path
    
    def read_file(self):
        in_file = open(self.input_file_path, "r")
        file_content = in_file.read()
        in_file.close()
        return file_content
    
    def process_file_content(self):
        file_content = self.read_file().replace(',', '')
        file_content = file_content.split()
        
        output = []
        current_coord = []
        # Applying the Python mentality «easier to ask forgiveness than
        # permission»
        for value in file_content:
            if value.replace('.', '').replace('-', '').isdigit():
                if isinstance(float(value), float):
                    try:
                        int(value)
                    except ValueError:
                        if len(current_coord) == 2:
                            output.append(current_coord)
                            current_coord = [float(value)]
                        else:
                            current_coord.append(float(value))
        output.append(current_coord)
        return output


if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("-i", "--inputfile", dest = "inputfile",
                      help = "input CSV file",
                      metavar = "FILE")
    parser.add_option("-o", "--outputfile", dest = "outputfile",
                      help = "output JSON file", metavar = "FILE")
    (options, args) = parser.parse_args()
    
    if options.inputfile and options.outputfile:
        converter = Converter(options.inputfile)
        file = open(options.outputfile, 'w+')
        file.write(str(converter.process_file_content()))
        file.close()
